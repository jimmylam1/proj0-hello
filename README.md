# Proj0-Hello
-------------

__Author__:  Jimmy Lam  
__Contact__: jimmyl@cs.uoregon.edu  

This is a basic program that prints "Hello world" to the terminal.
The file "hello.py" reads in the message from the file "credentials.ini"
and the message is then printed.

To run the code, use the command `make run` in the terminal.
